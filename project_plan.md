---
title: Final Project
subtitle: Project plan
authors: ['Iliyan Stoyadinov iliy0031@edu.ucl.dk']
date: \today
email: ['iliy0031@edu.ucl.dk']
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the final project for 4th semester. It will be made corporate network and differtent protocols and technologies will be used.


# Purpose


The main goal is to make a network on GNS3, that will contain of ISP network and 3 offices of a company. 
The purpose will be to connect the offices and provide enough redundancy to be sure that the network will not fail.


# Goals

My network will look like this:

![Network Diagram](NetworkDiagram.PNG)

* Office 3 is the main Office
* AS 5000 is the ISP network


* Make a low level diagram for the whole network.
* Configure OSPF in the ISP network and ensure connectivity between all routers.
* Configure DHCP on the gateway routers in the offices to give IP addresses to the computers. All computers must have IP addresses from the LLD.
* Configure eBGP between the ISP and the routers in the offices.
* Configure iBGP inside the ISP network.
* Configure MPLS with RSVP in the ISP network.
* Configure VRRP for redundancy in the main office to enusre connectivity if one of the routers fail.
* Implement QoS when everything is tested and it is working.
* Final project report will be made.


# Schedule

Project must be done before 1st of June.


# Organization

## Group:
*  Iliyan Stoyadinov iliy0031@edu.ucl.dk @iliyanstoyadinov



# Budget and resources

No monetary resources are expected. In terms of manpower, only i will contribute in the project.


# Risk assessment

## The most important risks we have to look out is:
*  Time Management
*  Electrical failiure
*  Lack of knowledge

## How to overcome these risks:
#### Time management:
* Do not leave all the work for the last week or so.
* Never leave issues unresolved. 
* Do only one task at a time.


#### Electrical failiure:

* Have backup. Make backup of the project on gitlab to save it if a laptop stop working.


#### Knowledge:
* Seek knowledge or ask technical supervisor for help.


# Stakeholders

Stakeholders are people who have direct contact with the project.

Internal Stakeholders:
* Lecturers


# Perspectives

This project will serve as a template for other similar project.
It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.

# Evaluation

On week 25 will be the exam wit hinternal and external examinator. 

# References

None at this time.
